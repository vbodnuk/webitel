package utile;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MsSqlConnector {

    private Connection connection;
    private String URL = "jdbc:sqlserver://ip;databaseName=Collaction;user=username;password=password";

    public MsSqlConnector() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException var3) {
            var3.printStackTrace();
        }

        try {
            this.connection = DriverManager.getConnection(this.URL);
        } catch (SQLException var2) {
            var2.printStackTrace();
        }

    }

    public Connection getConnection() {
        return this.connection;
    }
}
