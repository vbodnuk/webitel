package parser;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateToString {

    public static String date() throws Exception {
        Date dt = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String date = simpleDateFormat.format(dt);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(simpleDateFormat.parse(date));
        calendar.add(5, -1);
        String nDate = simpleDateFormat.format(calendar.getTime());
        return nDate;
    }
}
