package webitel;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class GetKey {

    private String key;
    private String token;

    public GetKey(String key, String token) {
        this.key = key;
        this.token = token;
    }

    public String getKey() {
        return this.key;
    }

    public String getToken() {
        return this.token;
    }

    public static GetKey keys() throws Exception {
        CloseableHttpClient client = HttpClientBuilder.create().build();
        JsonObject js = new JsonObject();
        js.addProperty("username", "name");
        js.addProperty("password", "password");
        HttpPost post = new HttpPost("https://wbt.kf.ua/engine/login");
        post.addHeader("content-type", "application/json");
        post.setEntity(new StringEntity(js.toString()));
        CloseableHttpResponse response = client.execute(post);
        BufferedReader bf = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuffer sb = new StringBuffer();

        String s;
        while((s = bf.readLine()) != null) {
            sb.append(s);
        }

        JsonParser parser = new JsonParser();
        JsonObject responsObject = (JsonObject)parser.parse(sb.toString());
        String t = String.valueOf(responsObject.get("key"));
        String k = String.valueOf(responsObject.get("token"));
        String token = t.substring(1, t.length() - 1);
        String key = k.substring(1, k.length() - 1);
        response.close();
        client.close();
        return new GetKey(token, key);
    }
}
