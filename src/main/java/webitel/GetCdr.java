package webitel;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class GetCdr {

    public JsonObject getCalls(String key, String token, long startDate, long endDate) throws Exception {
        CloseableHttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost("https://wbt.kf.ua/cdr/api/v2/cdr/text");
        post.addHeader("content-type", "application/json");
        post.addHeader("X-Access-Token", token);
        post.addHeader("X-Key", key);
        JsonParser parser = new JsonParser();
        InputStream is = this.getClass().getResourceAsStream("/request.json");
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        Object object = parser.parse(reader);
        JsonObject request = (JsonObject)object;
        request.getAsJsonArray("filter").get(0).getAsJsonObject().get("bool").getAsJsonObject().getAsJsonArray("must").get(0).getAsJsonObject().get("range").getAsJsonObject().get("created_time").getAsJsonObject().addProperty("gte", startDate);
        request.getAsJsonArray("filter").get(0).getAsJsonObject().get("bool").getAsJsonObject().getAsJsonArray("must").get(0).getAsJsonObject().get("range").getAsJsonObject().get("created_time").getAsJsonObject().addProperty("lte", endDate);
        post.setEntity(new StringEntity(request.toString()));
        CloseableHttpResponse response = client.execute(post);
        BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuffer sb = new StringBuffer();
        String line = "";

        while((line = br.readLine()) != null) {
            sb.append(line);
        }

        JsonObject jsResponse = (JsonObject)parser.parse(sb.toString());
        response.close();
        client.close();
        return jsResponse;
    }
}
