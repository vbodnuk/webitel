package webitel;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class GetAllData {

    public List<JsonObject> getData(JsonObject js, String key, String token) throws Exception {
        List<JsonObject> jsonObjectList = new ArrayList();
        int total = js.get("hits").getAsJsonObject().get("total").getAsInt();
        String id = js.get("_scroll_id").getAsString();
        jsonObjectList.add(js);
        CloseableHttpClient client = HttpClientBuilder.create().build();

        while(true) {
            total -= 999;
            if (total < 0) {
                client.close();
                return jsonObjectList;
            }

            HttpPost post = new HttpPost("https://wbt.kf.ua/cdr/api/v2/cdr/text/scroll");
            post.addHeader("content-type", "application/json");
            post.addHeader("X-Access-Token", token);
            post.addHeader("X-Key", key);
            JsonParser parser = new JsonParser();
            InputStream is = this.getClass().getResourceAsStream("/request_with_id.json");
            BufferedReader bf = new BufferedReader(new InputStreamReader(is));
            Object obj = parser.parse(bf);
            JsonObject jsonObject = (JsonObject)obj;
            jsonObject.addProperty("scrollId", id);
            post.setEntity(new StringEntity(jsonObject.toString()));
            bf.close();
            CloseableHttpResponse response = client.execute(post);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer();

            String line;
            while((line = reader.readLine()) != null) {
                sb.append(line);
            }

            JsonObject responsJs = (JsonObject)parser.parse(sb.toString());
            jsonObjectList.add(responsJs);
            id = responsJs.get("_scroll_id").getAsString();
            response.close();
            reader.close();
        }
    }
}
