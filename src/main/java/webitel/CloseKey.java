package webitel;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

public class CloseKey {

    public static void close(String key, String token) throws Exception {
        CloseableHttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost("https://wbt.kf.ua/engine/logout");
        post.addHeader("X-Access-Token", token);
        post.addHeader("X-Key", key);
        CloseableHttpResponse response = client.execute(post);
        response.close();
        client.close();
    }
}
