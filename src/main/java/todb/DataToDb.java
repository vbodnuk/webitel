package todb;

import com.google.gson.JsonObject;
import utile.MsSqlConnector;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

public class DataToDb {

    public static void insertData(List<JsonObject> jsList) throws Exception {
        DateTimeFormatter input = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        DateTimeFormatter out = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Connection connection = (new MsSqlConnector()).getConnection();
        connection.setAutoCommit(false);
        String query = "insert into Collaction.dbo.webitel_calls (created_time, uuid, destination_number, caller_number, gateway, direction, hangup_cause, duration, billsec, answersec, waitsec, holdsec, queue, sort)Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement ps = connection.prepareStatement(query);
        Savepoint savepoint = connection.setSavepoint();

        for(int i = 0; i < jsList.size(); ++i) {
            Object obj = jsList.get(i);
            JsonObject js = (JsonObject)obj;

            for(int j = 0; j < js.get("hits").getAsJsonObject().getAsJsonArray("hits").size(); ++j) {
                int duration = js.get("hits").getAsJsonObject().getAsJsonArray("hits").get(j).getAsJsonObject().get("fields").getAsJsonObject().getAsJsonArray("duration").get(0).getAsInt();
                String time = js.get("hits").getAsJsonObject().getAsJsonArray("hits").get(j).getAsJsonObject().get("fields").getAsJsonObject().getAsJsonArray("created_time").get(0).getAsString();
                String uuid = js.get("hits").getAsJsonObject().getAsJsonArray("hits").get(j).getAsJsonObject().get("fields").getAsJsonObject().getAsJsonArray("uuid").get(0).getAsString();
                String destNumber;
                if (js.get("hits").getAsJsonObject().getAsJsonArray("hits").get(j).getAsJsonObject().get("fields").getAsJsonObject().getAsJsonArray("destination_number") == null) {
                    destNumber = "null";
                } else {
                    destNumber = js.get("hits").getAsJsonObject().getAsJsonArray("hits").get(j).getAsJsonObject().get("fields").getAsJsonObject().getAsJsonArray("destination_number").get(0).getAsString();
                }

                String hangup = js.get("hits").getAsJsonObject().getAsJsonArray("hits").get(j).getAsJsonObject().get("fields").getAsJsonObject().getAsJsonArray("hangup_cause").get(0).getAsString();
                String cNumber = js.get("hits").getAsJsonObject().getAsJsonArray("hits").get(j).getAsJsonObject().get("fields").getAsJsonObject().getAsJsonArray("caller_id_number").get(0).getAsString();
                int bill = js.get("hits").getAsJsonObject().getAsJsonArray("hits").get(j).getAsJsonObject().get("fields").getAsJsonObject().getAsJsonArray("billsec").get(0).getAsInt();
                int answer = js.get("hits").getAsJsonObject().getAsJsonArray("hits").get(j).getAsJsonObject().get("fields").getAsJsonObject().getAsJsonArray("answersec").get(0).getAsInt();
                int wait = js.get("hits").getAsJsonObject().getAsJsonArray("hits").get(j).getAsJsonObject().get("fields").getAsJsonObject().getAsJsonArray("waitsec").get(0).getAsInt();
                int hold = js.get("hits").getAsJsonObject().getAsJsonArray("hits").get(j).getAsJsonObject().get("fields").getAsJsonObject().getAsJsonArray("holdsec").get(0).getAsInt();
                String queue;
                if (js.get("hits").getAsJsonObject().getAsJsonArray("hits").get(j).getAsJsonObject().get("fields").getAsJsonObject().getAsJsonArray("queue.name") == null) {
                    queue = "null";
                } else {
                    queue = js.get("hits").getAsJsonObject().getAsJsonArray("hits").get(j).getAsJsonObject().get("fields").getAsJsonObject().getAsJsonArray("queue.name").get(0).getAsString();
                }

                String d;
                if (js.get("hits").getAsJsonObject().getAsJsonArray("hits").get(j).getAsJsonObject().get("fields").getAsJsonObject().getAsJsonArray("direction") == null) {
                    d = "null";
                } else {
                    d = js.get("hits").getAsJsonObject().getAsJsonArray("hits").get(j).getAsJsonObject().get("fields").getAsJsonObject().getAsJsonArray("direction").get(0).toString();
                }

                String direction = d.substring(1, d.length() - 1);
                long sort = js.get("hits").getAsJsonObject().getAsJsonArray("hits").get(j).getAsJsonObject().get("sort").getAsLong();
                LocalDateTime date = LocalDateTime.parse(time, input);
                date = date.plusHours(2L);
                String createdtime = out.format(date);
                Date dt = sdf.parse(createdtime);
                Timestamp sqlTime = new Timestamp(dt.getTime());
                ps.setTimestamp(1, sqlTime);
                ps.setString(2, uuid);
                String f = String.valueOf(destNumber.charAt(0));
                String gateway;
                if (f.equals("+")) {
                    gateway = destNumber.substring(1, destNumber.length());
                    ps.setString(3, gateway);
                } else if (destNumber.length() == 10) {
                    gateway = String.valueOf(destNumber.substring(0, 4));
                    if (gateway.equals("0800")) {
                        ps.setString(3, destNumber);
                    } else {
                        String number2 = "38" + destNumber;
                        ps.setString(3, number2);
                    }
                } else {
                    ps.setString(3, destNumber);
                }

                if (cNumber.length() == 10) {
                    gateway = "38" + cNumber;
                    ps.setString(4, gateway);
                } else if (cNumber.length() == 13) {
                    gateway = cNumber.substring(1, cNumber.length() - 1);
                    ps.setString(4, gateway);
                } else if (cNumber.length() == 9) {
                    (new StringBuilder()).append("380").append(cNumber).toString();
                } else {
                    ps.setString(4, cNumber);
                }

                if (js.get("hits").getAsJsonObject().getAsJsonArray("hits").get(j).getAsJsonObject().get("fields").getAsJsonObject().getAsJsonArray("gateway") == null) {
                    gateway = "null";
                } else {
                    gateway = js.get("hits").getAsJsonObject().getAsJsonArray("hits").get(j).getAsJsonObject().get("fields").getAsJsonObject().getAsJsonArray("gateway").get(0).getAsString();
                }

                ps.setString(5, gateway);
                ps.setString(6, direction);
                ps.setString(7, hangup);
                ps.setInt(8, duration);
                ps.setInt(9, bill);
                ps.setInt(10, answer);
                ps.setInt(11, wait);
                ps.setInt(12, hold);
                ps.setString(13, queue);
                ps.setLong(14, sort);
                ps.addBatch();
            }
        }

        try {
            ps.executeBatch();
            ps.clearBatch();
            ps.clearParameters();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback(savepoint);
        } finally {
            ps.close();
            connection.close();
        }

    }
}
