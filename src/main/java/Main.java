import com.google.gson.JsonObject;
import parser.DateToString;
import todb.DataToDb;
import webitel.CloseKey;
import webitel.GetAllData;
import webitel.GetCdr;
import webitel.GetKey;

import java.text.SimpleDateFormat;
import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
        String date = DateToString.date();
        long startDate = (new SimpleDateFormat("dd/MM/yyyy HH:mm")).parse(date + " 00:00").getTime();
        long endDate = (new SimpleDateFormat("dd/MM/yyyy HH:mm")).parse(date + " 23:59").getTime();
        GetKey keys = GetKey.keys();
        GetCdr cdr = new GetCdr();
        JsonObject js = cdr.getCalls(keys.getKey(), keys.getToken(), startDate, endDate);
        GetAllData data = new GetAllData();
        List<JsonObject> list = data.getData(js, keys.getKey(), keys.getToken());
        DataToDb.insertData(list);
        CloseKey.close(keys.getKey(), keys.getToken());
    }
}
